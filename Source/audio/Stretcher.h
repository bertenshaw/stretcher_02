//
//  Stretcher.h
//

#ifndef H_Stretcher
#define H_Stretcher

#include "../JuceLibraryCode/JuceHeader.h"

/**
 This muli-channel audio stretcher interpollates values between audio samples when
 stretched producing a grainy unique timestrectching effect
 */
class Stretcher
{
public:
// Constructor
    Stretcher();
    
//  Destructor
    ~Stretcher();
    
// Allows playback of stretched material
    void setPlayState (bool newState);
    
// Checks the if playback is currently happening
    bool getPlayState() const;
    
// Updates the degree of stretching
    void setStretchScale (bool newState);
    
//    /**
//     Gets the current record state of the stretcher
//     */
//    bool getRecordState() const;
    
// Processes each sample for playback
    float processSample (float input);

private:
    //Audio data
    static const int bufferSize = 88200; //constant buffer size
    unsigned int bufferPosition;
    float audioSampleBuffer[bufferSize];
};

#endif /* H_Stretcher */
