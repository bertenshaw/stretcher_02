/*
  ==============================================================================

    Audio.h

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Stretcher.h"

class Audio :   public AudioIODeviceCallback

{
public:
    // Constructor
    Audio();
    
    // Destructor
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    
    Stretcher& getStretcher() { return stretcher; }
    
//    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
private:
    AudioDeviceManager audioDeviceManager;
    Stretcher stretcher;
};



#endif  // AUDIO_H_INCLUDED
