//
//  StretherGui.h
//

#ifndef H_StretcherGui
#define H_StretcherGui

#include "../JuceLibraryCode/JuceHeader.h"
#include "Stretcher.h"

// Gui for Stretcher
class StretcherGui :   public Component,
                    public Button::Listener
{
public:
    // Contructor
    StretcherGui(Stretcher& stretcher_);
    
    //Button Listener
    void buttonClicked (Button* button);
    
    //Component
    void resized();
private:
    Stretcher& stretcher;
    TextButton playButton;
    TextButton recordButton;
};

#endif
