//
//  StretcherGui.cpp
//

#include "StretcherGui.h"

StretcherGui::StretcherGui(Stretcher& stretcher_) : stretcher (stretcher_)
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
}

void StretcherGui::buttonClicked (Button* button)
{
//    if (button == &playButton)
//    {
//        looper.setPlayState (!looper.getPlayState());
//        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
//        if (looper.getPlayState())
//            playButton.setButtonText ("Stop");
//        else
//            playButton.setButtonText ("Play");
//    }
//    else if (button == &recordButton)
//    {
//        looper.setRecordState (!looper.getRecordState());
//        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);
//
//    }
}

void StretcherGui::resized()
{
//    playButton.setBounds (0, 0, getWidth()/2, getHeight()/2);
//    recordButton.setBounds (playButton.getBounds().translated(getWidth()/2, 0));
}